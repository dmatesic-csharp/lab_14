﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace SocketServer {
    class Program {
        static void Main(string[] args) {
            TcpListener server = new TcpListener(IPAddress.Any, 50000);
            server.Start();

            while (true) {
                Console.WriteLine("Čekam na poruku...");
                TcpClient klijent = server.AcceptTcpClient();

                NetworkStream stream = klijent.GetStream();
                byte[] poruka = new byte[1024];
                stream.Read(poruka, 0, 100);
                Console.WriteLine(DateTime.Now + " - primljena poruka");
                string porukaString = Encoding.ASCII.GetString(poruka).TrimEnd('\0');
                Console.WriteLine("Poruka: " + porukaString);
                klijent.Close();
            }
        }
    }
}
