namespace SocketKlijentGUI {
    public partial class FormMain : Form {
        public Klijent klijent;
        public FormMain() {
            InitializeComponent();
            klijent = new Klijent();
            
        }

        private void btnSend_Click(object sender, EventArgs e) {
            string ipAdresa = tbServerIP.Text;
            int port = int.Parse(tbServerPort.Text);
            string poruka = tbMessage.Text;
            klijent.salji(ipAdresa, port, poruka);
        }
    }



}