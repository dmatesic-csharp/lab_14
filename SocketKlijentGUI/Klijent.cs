﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace SocketKlijentGUI {
    public class Klijent {
        public void salji(string ipAdresa, int port, string poruka) {
            
                TcpClient klijent = new TcpClient(ipAdresa, port);
                Byte[] msg = Encoding.ASCII.GetBytes(poruka);
                NetworkStream stream = klijent.GetStream();
                Console.WriteLine(DateTime.Now + " - šaljem poruku!");
                stream.Write(msg, 0, msg.Length);
                stream.Close();
                Console.WriteLine(DateTime.Now + " - poruka poslana!");
                klijent.Close();
            
        }
    }
}

